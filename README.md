
# VennDiagram R package

This is a fork of the [CRAN VennDiagram package](https://CRAN.R-project.org/package=VennDiagram).  It fixes the bug where the filename is not allowed to be set to NULL, and adds some minor features to make it easier to style the plot and run in an interactive session.

[Package manual with examples](doc/Reference_Manual_VennDiagram.md)

## Installation

You can install this version of VennDiagram with:

``` r
remotes::install_gitlab("pooranis/VennDiagram")
```

You can install the upstream [CRAN](https://CRAN.R-project.org) version of VennDiagram with:

``` r
install.packages("VennDiagram")
```

## Citation

To cite VennDiagram in publications use:

Chen H, Boutros PC (2011). "VennDiagram: a package for the generation of
highly-customizable Venn and Euler diagrams in R." _BMC Bioinformatics_, *12*(1), 35. ISSN
1471-2105, doi: [10.1186/1471-2105-12-35](https://doi.org/10.1186/1471-2105-12-35).

## Note from the original authors:

The VennDiagram package is copyright (c) 2012 Ontario Institute for Cancer Research (OICR)
This package and its accompanying libraries is free software; you can redistribute it and/or modify it under the terms of the GPL (either version 1, or at your option, any later version) or the Artistic License 2.0.  Refer to LICENSE for the full license text.

OICR makes no representations whatsoever as to the SOFTWARE contained herein.  It is experimental in nature and is provided WITHOUT WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER WARRANTY, EXPRESS OR IMPLIED. OICR MAKES NO REPRESENTATION OR WARRANTY THAT THE USE OF THIS SOFTWARE WILL NOT INFRINGE ANY PATENT OR OTHER PROPRIETARY RIGHT. By downloading this SOFTWARE, your Institution hereby indemnifies OICR against any loss, claim, damage or liability, of whatsoever kind or nature, which may arise from your Institution's respective use, handling or storage of the SOFTWARE. If publications result from research using this SOFTWARE, we ask that the Ontario Institute for Cancer Research be acknowledged and/or credit be given to OICR scientists, as scientifically appropriate.



